/////////////////////////////////////////////////////////////////////////////////////////////
//
// string-pad-right
//
//    Modifies a string to have a given number of a specified character at the end of a string.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var type =  require("type");

function padRight(str, length, char) {
    return !type.isString(str) || length <= str.length? str : str + (new Array((length - str.length) + 1)).join(char || " ");
}


///////////////////////////////////////////////////////////////////////////////////////////// 
module.exports = padRight;